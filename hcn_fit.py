#!/bin/python

import os
import pandas
import numpy
import pyglet
import geoplotlib
import shutil
from geoplotlib.utils import read_csv, epoch_to_str, BoundingBox
from geoplotlib.colors import colorbrewer

#file array
f=[]

#import key file
key=open("NEnames.txt").read().splitlines()

#file extension type
ext='.dly'

#get directory
mypath = os.getcwd()

#look at files available in the current directory
for (dirpath, dirnames, filenames) in os.walk(mypath):
    f.extend(filenames)
    break

#list of lat and lon by NElats
vot = pandas.read_csv('NElats.csv').to_records()

#arrays for storing various things
files=[]
stats=[]
vals=[]

#check for available data for the NE stations
for l in f:
    for k in key:
        if(l == k+ext):
            files.append(l)
            vals.append(k)

#Simplify list of Lat and Lon based on available data
for j in vals:
    for i in range(0, len(vot)):
        if(j==vot[i][1]):
            stats.append([vot[i][1],vot[i][2],vot[i][3]])

#create available stations file
flat=open("avail_stat.csv","w")

#write to available stations file
flat.write('name,lat,lon \n')
flat.write('\n'.join('{},{},{}'.format(x[0],x[1],x[2])  for x in stats))
flat.close()

#open plotting files
tot = read_csv('NElats.csv')
avail = read_csv('avail_stat.csv')

#plot using geoplotlib
geoplotlib.kde(tot, bw=5)
geoplotlib.dot(avail,'r')
geoplotlib.labels(avail, 'name', color=[0,0,255,255], font_size=14, anchor_x='center')
geoplotlib.show()
    
