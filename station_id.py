#!/bin/python
#T. Jacovich
#For ECE 6130


import numpy
import matplotlib.pyplot as plt
import pyglet
import geoplotlib
from geoplotlib.utils import read_csv, epoch_to_str, BoundingBox
from geoplotlib.colors import colorbrewer
import pandas
import ghcnpy as gp
import plotly.graph_objs as go
import plotly.figure_factory as FF
import plotly.plotly as py
#file containing HCN station names
f='ghcnd-stations.txt'

#extracts lines from file
lines = open(f).read().splitlines()

#divide lines into entries and put them in words
words =[]

for j in range(0, len(lines)):
     words.append(lines[j].split(" "))

#dictionary for selecting lines
key=['CT','MA', 'VT', 'RI','NY','ME']

#station container list
stats=[]
keys=[]

ffinal=open("NEnames.txt","w")
for i in key:
    ffinal.write(i)
    ffinal.write('\n')

#search words for instances of key
for i in key:
    stats=[]
    keys=[]
    for j in range(0, len(words)):
        if(words[j][8]==i):
            stats.append([words[j][0], words[j][2], words[j][4]])

    #generate dict for station search
    for k in range(0,len(stats)):        
        keys.append(stats[k][0])
    
    #create output file
    foutname='../NEnames%s.txt' %i
    flatname='../NElats%s.csv' %i
    fout=open(foutname, "w")
    flat=open(flatname,"w")

    #write keys to output
    for i in range(0,len(keys)):
        fout.write(keys[i])
        fout.write("\n")

    #write values to file for plotting
    flat.write('name,lat,lon\n')
    flat.write("\n".join('{},{},{}'.format(x[0],x[1],x[2])  for x in stats))

    flat.close()

    pull csv files for stations.
    for j in keys:
        outfile=gp.get_data_station(j)
        print(outfile,"has been downloaded")
        gp.output_to_csv(j)

import csv

#imports csv 
#for j in keys:
#    outfile = '%s.csv' %j
#    outfile2= 't%s.csv' %j
#    print('%s\n' %outfile2)
#    ftrue = open(outfile2, "w")
#    ftrue.write('DATE, TMAX, TMIN, PRCP, SNOW, SNWD\n')

#    with open(outfile) as csvfile:
#        ffix=csv.DictReader(csvfile)
#        for row in ffix:
#            s="-"
#            seq=(row['YYYY'], row['MM'], row['DD'])
#            date= s.join(seq)
#            ftrue.write("".join('{},{},{},{},{},{}'.format(date,row['TMAX'],row['TMIN'],row['PRCP'],row['SNOW'],row['SNWD'])))
#            ftrue.write("\n")
#    ftrue.close()

#generate geographic plot
tot = read_csv('NElatsCT.csv')
geoplotlib.dot(tot,'r')
geoplotlib.labels(tot, 'name', color=[0,0,255,255], font_size=10, anchor_x='center')
geoplotlib.show()

