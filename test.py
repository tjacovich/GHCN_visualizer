

#T. Jacovich 5/13/18

import ghcnpy as gp
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
from datetime import date


#__name__='NOAA Historical Climatological Data'


#initialization call
app = dash.Dash()

#css style
app.css.append_css({'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'})

#initial data
available = open('NEnames.txt')
available_states = available.read().splitlines()
available_names = open('NEnamesCT.txt')
df = pd.read_csv('csv_data/USW00014740.csv')
df = df
keys = df.keys()
dater=[]
dater1=[]
daterange=pd.read_csv('daterange.csv')
tmaxdat=pd.read_csv('tmaxUSW00014740.csv')
tmindat=pd.read_csv('tminUSW00014740.csv')
prcpdat=pd.read_csv('prcpUSW00014740.csv')
dateranger=[]
dkey=daterange.keys()
nos=list(range(1,100,5))
#put date in datetime form
for i in range(0, len(df)):
    yyyy = df[keys[0]][i]
    mm = df[keys[1]][i]
    dd = df[keys[2]][i]
    dater.append(date(yyyy,mm,dd))
    dater1.append(date(2000,mm,dd))
useful_set = zip(dater1,df[keys[3]])

for i in range(0, len(daterange)):
    yyyy=2020
    mm=daterange[dkey[0]][i]
    dd=daterange[dkey[1]][i]
    dateranger.append(date(yyyy,mm,dd))

useful_set.sort()
available_indicators = keys[3:]
available_files = available_names.read().splitlines()

#app layout
app.layout = html.Div([  
    #top header
    html.H1(
        children='A Web-based Tool for Visualizing Global Historical Climatological Network Data from NOAA Stations',
        style={
        }
    ),
    #top dropdown menus
    html.Div([
        dcc.Dropdown(
            id='state-name',
            options=[{'label': i, 'value': i} for i in available_states],
            value='CT'
            ),

        dcc.Dropdown(
            id='file-value',
            options=[{'label': i, 'value': i} for i in available_files],
            value='USW00014740'
            ),

        html.Div([
            dcc.Dropdown(
                id='crossfilter-xaxis-column',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value=available_indicators[0]
            ),
            dcc.RadioItems(
                id='crossfilter-xaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ],
        style={'width': '49%', 'display': 'inline-block'}),

        html.Div([
            dcc.Dropdown(
                id='crossfilter-yaxis-column',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value=available_indicators[1]
            ),
            dcc.RadioItems(
                id='crossfilter-yaxis-type',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ], style={'width': '49%', 'float': 'right', 'display': 'inline-block'})
    ], style={
        'borderBottom': 'thin lightgrey solid',
        'backgroundColor': 'rgb(250, 250, 250)',
        'padding': '10px 5px'
    }),

    #main scatterplot
    html.Div([
        dcc.Graph(
            id='crossfilter-indicator-scatter',
            hoverData={'points': [{'customdata': 'date'}]}
        )
    ], style={'width': '49%', 'display': 'inline-block', 'padding': '0 20'}),
    
    #time series data
    html.Div([
        dcc.Graph(id='x-time-series'),
        dcc.Graph(id='y-time-series'),
    ], style={'display': 'inline-block', 'width': '49%'}),

    #year slider
    html.H4(children='Year Selection Slider',style={}),
    html.Div(dcc.Slider(
        id='crossfilter-year--slider',
        min=df[keys[0]][::100].min(),
        max=df[keys[0]][::100].max(),
        value=df[keys[0]][::100].max(),
        step=300,
        marks={str(year): str(year) for year in df[keys[0]][::700].unique()} 
    ), style={'width': '95%', 'padding': '0px 20px 20px 20px'}),

    #interval sliders
    html.H4(children='Date Interval Selection Slider',style={}),
    html.Div(dcc.Slider(
        id='crossfilter-day--slider',
        min=1,
        max=100,
        value=30,
        step=300,
        marks={str(year): str(year) for year in nos} 
    ), style={'width': '65%', 'padding': '0px 20px 20px 20px'}),

    html.H3(children='Values Plotted by Day for entire dataset',style={}),
        #big time series
        html.Div([
            dcc.Dropdown(
                id='crossfilter-xaxis-column-big',
                options=[{'label': i, 'value': i} for i in available_indicators],
                value=available_indicators[0]
            ),
            dcc.RadioItems(
                id='crossfilter-xaxis-type-big',
                options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                value='Linear',
                labelStyle={'display': 'inline-block'}
            )
        ],
        style={'width': '60%', 'display': 'inline-block'}),


    html.Div([
        dcc.Graph(id='big-time-series'),
    ], style={'display': 'inline-block', 'width': '60%'}),
    html.H2(children='Move The Sliders to Decide What Kind of Day You are Looking for',style={}),
    html.Div([
        dcc.Dropdown(
                id='crossfilter-xaxis-prob',
                options=[{'label': i, 'value': i} for i in {"USW00017470"}],
                value="USW00017470"
            )],style={'width': '60%', 'display': 'inline-block'}),
    
    html.H4(children='Maximum Temperature Slider (C)',style={}),
    html.Div(dcc.Slider(
        id='tmax--slider',
        min=-40,
        max=40,
        value=20,
        step=1,
        marks={str(i): str(i) for i in range(-40,40)} 
    ), style={'width': '75%', 'padding': '0px 20px 20px 20px'}),


            html.H4(children='Minimum Temperature Slider (C)',style={}),
    html.Div(dcc.Slider(
        id='tmin--slider',
        min=-40,
        max=40,
        value=10,
        step=1,
        marks={str(i): str(i) for i in range(-40,40)} 
    ), style={'width': '75%', 'padding': '0px 20px 20px 20px'}),

    html.H4(children='Maximum Allowed Percipitation Slider(mm)',style={}),
    html.Div(dcc.Slider(
        id='pmax--slider',
        min=0.,
        max=10.,
        value=0.2,
        step=.2,
        marks={str(i/5.): str(i/5.) for i in range(0,50)} 
    ), style={'width': '75%', 'padding': '0px 20px 20px 20px'}),

    html.H4(children='Minimum Allowed Percipitation Slider(mm)',style={}),
    html.Div(dcc.Slider(
        id='pmin--slider',
        min=0.,
        max=10.,
        value=0.,
        step=.2,
        marks={str(i/5.): str(i/5.) for i in range(0,50)} 
    ), style={'width': '75%', 'padding': '0px 20px 20px 20px'}),

    html.Div([
        dcc.Graph(id='prob-time-series'),
    ], style={'display': 'inline-block', 'width': '60%'}),
])


###################################################################################################################
#                     update functions
#
##################################################################################################################
def update_file(filename):    
    df = pd.read_csv('csv_data/%s.csv' %filename)
    df = df
    keys = df.keys()
    dater=[]
    for i in range(0, len(df)):
        yyyy = df[keys[0]][i]
        mm = df[keys[1]][i]
        dd = df[keys[2]][i]
        dater.append(date(yyyy,mm,dd))
        available_indicators = keys[3::]
    return df, dater

@app.callback(
    dash.dependencies.Output('file-value', 'options'),
    [dash.dependencies.Input('state-name','value')])
    
def update_stations(statename):
    available_names = open("NEnames%s.txt" %statename)
    available_files = available_names.read().splitlines()
    return [{'label': i, 'value': i} for i in available_files]

@app.callback(
    dash.dependencies.Output('crossfilter-indicator-scatter', 'figure'),
    [dash.dependencies.Input('file-value','value'),
     dash.dependencies.Input('crossfilter-xaxis-column', 'value'),
     dash.dependencies.Input('crossfilter-yaxis-column', 'value'),
     dash.dependencies.Input('crossfilter-xaxis-type', 'value'),
     dash.dependencies.Input('crossfilter-yaxis-type', 'value'),
     dash.dependencies.Input('crossfilter-year--slider', 'value'),
     dash.dependencies.Input('crossfilter-day--slider', 'value')])


def update_graph(filename, xaxis_column_name, yaxis_column_name,
                 xaxis_type, yaxis_type,
                 year_value, day_value):

    df, dater = update_file(filename)

    return {
        'data': [go.Scattergl(
            x=df[xaxis_column_name][:365*(year_value-min(dater).year)][::day_value],
            y=df[yaxis_column_name][:365*(year_value-min(dater).year)][::day_value],
            text = dater[:365*(year_value-min(dater).year)][::day_value],
            customdata = dater[:365*(year_value-min(dater).year)][::day_value],
            mode='markers',
            marker={
                'size': 15,
                'opacity': 0.5,
                'line': {'width': 0.5, 'color': 'white'}
            }
        )],
        'layout': go.Layout(
            xaxis={
                'title': xaxis_column_name,
                'type': 'linear' if xaxis_type == 'Linear' else 'log'
            },
            yaxis={
                'title': yaxis_column_name,
                'type': 'linear' if yaxis_type == 'Linear' else 'log'
            },
            margin={'l': 40, 'b': 30, 't': 10, 'r': 0},
            height=450,
            hovermode='closest'
        )
    }


def create_time_series(df, dater, axis_type, title, year_value, day_value):
    return {
        'data': [go.Scattergl(
            x=dater[:365*(year_value-min(dater).year)][::day_value],
            y=df[title][:365*(year_value-min(dater).year)][::day_value],
            mode='lines+markers'
        )],
        'layout': {
            'height': 225,
            'margin': {'l': 20, 'b': 30, 'r': 10, 't': 10},
            'annotations': [{
                'x': 0, 'y': 0.85, 'xanchor': 'left', 'yanchor': 'bottom',
                'xref': 'paper', 'yref': 'paper', 'showarrow': False,
                'align': 'left', 'bgcolor': 'rgba(255, 255, 255, 0.5)',
                'text': title
            }],
            'yaxis': {'type': 'linear' if axis_type == 'Linear' else 'log'},
            'xaxis': {'showgrid': False}
        }
    }


@app.callback(
    dash.dependencies.Output('x-time-series', 'figure'),
    [dash.dependencies.Input('file-value','value'),
    dash.dependencies.Input('crossfilter-indicator-scatter', 'hoverData'),
     dash.dependencies.Input('crossfilter-xaxis-column', 'value'),
     dash.dependencies.Input('crossfilter-xaxis-type', 'value'),
     dash.dependencies.Input('crossfilter-year--slider', 'value'),
     dash.dependencies.Input('crossfilter-day--slider', 'value')])

def update_x_timeseries(filename, hoverData, xaxis_column_name, axis_type, year_value, day_value):
    title = xaxis_column_name
    df, dater = update_file(filename)
    return create_time_series(df, dater, axis_type, title, year_value, day_value)


@app.callback(
    dash.dependencies.Output('y-time-series', 'figure'),
    [dash.dependencies.Input('file-value','value'),
     dash.dependencies.Input('crossfilter-indicator-scatter', 'hoverData'),
     dash.dependencies.Input('crossfilter-yaxis-column', 'value'),
     dash.dependencies.Input('crossfilter-yaxis-type', 'value'),
     dash.dependencies.Input('crossfilter-year--slider', 'value'),
     dash.dependencies.Input('crossfilter-day--slider', 'value')])


def update_y_timeseries(filename, hoverData, yaxis_column_name, axis_type, year_value, day_value):
    df, dater = update_file(filename)
    return create_time_series(df, dater, axis_type, yaxis_column_name, year_value, day_value)

###################################################################################################################     
#                                      big time series
#
##################################################################################################################

def create_big_time_series(df, dater, axis_type, title, year_value):
    return {
        'data': [go.Scattergl(
            x=dater1[:365*(year_value-min(dater).year)],
            y=df[title][:365*(year_value-min(dater).year)],
            mode='markers'
        )],
        'layout': {
            'height': 225,
            'margin': {'l': 20, 'b': 30, 'r': 10, 't': 10},
            'annotations': [{
                'x': 0, 'y': 0.85, 'xanchor': 'left', 'yanchor': 'bottom',
                'xref': 'paper', 'yref': 'paper', 'showarrow': False,
                'align': 'left', 'bgcolor': 'rgba(255, 255, 255, 0.5)',
                'text': title
            }],
            'yaxis': {'type': 'linear' if axis_type == 'Linear' else 'log'},
            'xaxis': {'showgrid': False}
        }
    }
@app.callback(
    dash.dependencies.Output('big-time-series', 'figure'),
    [dash.dependencies.Input('file-value','value'),
     dash.dependencies.Input('crossfilter-xaxis-column-big', 'value'),
     dash.dependencies.Input('crossfilter-xaxis-type-big', 'value'),
     dash.dependencies.Input('crossfilter-year--slider', 'value')
])

def update_big_timeseries(filename, xaxis_column_name, xaxis_type,year_value):
    title = xaxis_column_name
    df, dater = update_file(filename)
    return create_big_time_series(df, dater, xaxis_type, title, year_value)


##################################################################################################################
#                                   Monte Carlo Predictions
#
##################################################################################################################



def determine_percentages(tmin,tmax,pmin,pmax):
    bob=[]
    for tkey in tmaxdat.keys():
        temp=0

        for i in range(0,1000):
            if tmaxdat[tkey][i]<=tmax and tmaxdat[tkey][i]>=tmin and  pmin<=prcpdat[tkey][i] and prcpdat[tkey][i]<=pmax:
                temp+=1
        bob.append(temp/1000.)
    return bob

def create_prob_time_series(tmin,tmax,pmax,pmin,title):
    return {
        'data': [go.Scattergl(
            x=dateranger,
            y=determine_percentages(tmin,tmax,pmin,pmax),
            mode='lines+markers'
        )],
        'layout': {
            'height': 225,
            'margin': {'l': 20, 'b': 30, 'r': 10, 't': 10},
            'annotations': [{
                'x': 0, 'y': 0.85, 'xanchor': 'left', 'yanchor': 'bottom',
                'xref': 'paper', 'yref': 'paper', 'showarrow': False,
                'align': 'left', 'bgcolor': 'rgba(255, 255, 255, 0.5)',
                'text': title
            }],
            'yaxis': {},
            'xaxis': {}
        }
    }

@app.callback(
    dash.dependencies.Output('prob-time-series', 'figure'),
    [dash.dependencies.Input('tmax--slider','value'),
     dash.dependencies.Input('tmin--slider', 'value'),
     dash.dependencies.Input('pmax--slider', 'value'),
     dash.dependencies.Input('pmin--slider', 'value')
])


def update_prob_timeseries(tmax,tmin,pmax,pmin):
    title = "Probability of the Day You Requested"
    return create_prob_time_series(tmin,tmax,pmax,pmin, title)


if __name__ == '__main__':
    app.run_server()
